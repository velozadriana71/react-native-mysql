import { Text, StyleSheet, TouchableOpacity, TextInput } from "react-native";
import React, { useState, useEffect } from "react";
import Layout from "../components/Layout";

import { saveTask, getTask, updateTask } from "../api";

const TaskFormScreen = ({ navigation, route }) => {
  const [task, setTask] = useState({
    title: "",
    description: "",
  });
  const [editing, setEditing] = useState(false);

  useEffect(() => {
    if (route.params && route.params.id) {
      setEditing(true);
      navigation.setOptions({ headerTitle: "Editar tarea" });
      (async () => {
        const task = await getTask(route.params.id);
        setTask({ title: task.title, description: task.description });
      })();
    }
  }, []);

  const handleSubmit = async() => {
    try{
     if(!editing) {
       await saveTask(task);
      }else{
       await updateTask(route.params.id, {...task});
      }
      navigation.navigate("HomeScreen");
    }
    catch (error) {
     console.log("error")
    }
   };

   const handleChange = (name, value) => setTask({ ...task, [name]: value });

  return (
    <Layout>
      <TextInput
        style={styles.input}
        placeholder="Escribe el titulo:"
        value={task.title}
        onChangeText={(text) => handleChange("title", text)}
        
      />
      <TextInput
        style={styles.input}
        placeholder="Escribe una descripción:"
        value={task.description}
        onChangeText={(text) => handleChange("description", text)}
        
      />
  {!editing ? (
      <TouchableOpacity style={styles.ButtonSave} onPress={handleSubmit}>
        <Text style={styles.buttontext}>Guardar Tarea</Text>
      </TouchableOpacity>
    ) : (
      <TouchableOpacity style={styles.ButtonUpdate} onPress={handleSubmit}>
        <Text style={styles.buttontext}>Editar Tarea</Text>
      </TouchableOpacity>
    )}
      
    </Layout>
  );
};

const styles = StyleSheet.create({
  input: {
    width: "90%",
    marginBottom: 12,
    fontSize: 16,
    borderWidth: 1,
    borderColor: "#B3FFE2",
    height: 35,
    padding: 4,
    textAlign: "center",
    borderRadius: 10,
  },
  ButtonSave: {
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 10,
    marginBottom: 3,
    backgroundColor: "#B3FFE2",
    width: "90%",
  },
  buttontext: {
    color: "#707976",
    textAlign: "center",
  },
  ButtonUpdate:{
    padding: 10,
    paddingBottom: 10,
    borderRadius: 10,
    marginBottom: 3,
    backgroundColor: "#E58223",
    width: "90%"
  }
});

export default TaskFormScreen;
