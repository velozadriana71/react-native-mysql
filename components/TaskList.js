import React, { useState, useEffect } from "react";
import { FlatList, RefreshControl } from "react-native";
import { useIsFocused } from "@react-navigation/native";
import { getTasks, deleteTask } from "../api";
import TaskItem from "./TaskItem";

const TaskList = () => {
  const [tasks, setTasks] = useState([]);
  const [refreshing, setRefresing] = useState(false);

  const idFocused = useIsFocused()

  const loadTasks = async () => {
    const data = await getTasks();
    //console.log(data)
    setTasks(data.data);
  };

  useEffect(() => {
    loadTasks();
  }, [useIsFocused]);

  const handleDelete = async (id) => {
    await deleteTask(id);
    await loadTasks();
  };

  const renderItem = ({ item }) => {
    return <TaskItem task={item} handleDelete={handleDelete} />;
  };

  const onRefresh = React.useCallback(async () => {
    setRefresing(true);
    await loadTasks();
    setRefresing(false);
  });

  return (
    <FlatList
      style={{ width: "100%" }}
      data={tasks}
      keyExtractor={(item) => item.id + ""}
      renderItem={renderItem}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          colors={["#FBFF86"]}
          onRefresh={onRefresh}
          progressBackgroundColor="#FFDAF9"
        />
      }
    />
  );
};

export default TaskList;
