import { View, Text, StyleSheet, StatusBar } from "react-native";
import React from "react";

const Layout = ({ children }) => {
  return <View style={styles.containter}>
    <StatusBar backgroundColor= "#FF8DE7"/>
    {children}</View>;
};
const styles = StyleSheet.create({
  containter: {
    backgroundColor: "#FFF4FD",
    padding: 20,
    flex: 1,
    alignItems: "center",
  },
});

export default Layout;
