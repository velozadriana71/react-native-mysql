import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

const TaskItem = ({task, handleDelete}) => {
  const navigation = useNavigation();
  return (
    <View style={styles.ItemContainer}>
     <TouchableOpacity onPress={() =>navigation.navigate('TaskFormScreen', {id: task.id})}>
     <Text>{task.title}</Text>
      <Text>{task.description}</Text>
     </TouchableOpacity>

      <TouchableOpacity style={{backgroundColor:"#FFD5FC", padding: 7, borderRadius: 5}}
      onPress={() => handleDelete(task.id)}>
        
        <Text style={styles.itemTitle}>Eliminar</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
    ItemContainer: {
        backgroundColor: "#FDFFDC",
        padding: 20,
        marginVertical: 8,
        borderRadius: 10,
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: "center"
    },
    itemTitle: {
      color: "#FF3333",
    }
});

export default TaskItem