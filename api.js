const API = "http://192.168.1.77:4000/api/tasks";

//Obtener datos
export const getTasks = async () => {
  const res = await fetch(API);
  return await res.json();
};

//Obtener una sola tarea
export const getTask = async(id) =>{
  const res = await fetch(`${API}/${id}`);
  return await res.json();
}

//Guardar datos
export const saveTask = async (newTask) => {
  const res = await fetch(API, {
    method: "POST",
    headers: { Accept: "application/json", "Content-Type": "application/json" },
    body: JSON.stringify(newTask),
  });
  return await res.json();
};

//Eliminar tareas

export const deleteTask = async (id) => {
  await fetch(`${API}/${id}`, {
    method: "DELETE",
  });
};

//Editar tarea
export const updateTask = async(id, newTask) =>{
 const res = await fetch(`${API}/${id}`, {
    method: "PUT",
    headers: { Accept: "application/json", "Content-Type": "application/json" },
    body: JSON.stringify(newTask)
  });
  return res;
}


